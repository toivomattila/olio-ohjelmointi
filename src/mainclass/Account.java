//Toivo Mattila (0452499)
//2016-10-12

package mainclass;

public class Account {
    protected String no;
    protected int balance;
    
    public Account(){
        no = "";
        balance = 0;
    }
    public Account(String n, int b){
        no = n;
        balance = b;
    }
    
    public void depositMoney(int amount){
        balance += amount;
    }
    
    public void withdrawMoney(int amount){
        balance -= amount;
    }
    
    public String getAccountNo(){
        return no;
    }
    public int getBalance(){
        return balance;
    }
    public int getCreditLimit(){return 0;}
}

class CommonAccount extends Account{
    
    public CommonAccount(){
        super();
    }
    
    public CommonAccount(String n, int b){
        super(n, b);
    }
    
    public void withdrawMoney(int amount){
        if(balance - amount >= 0){
            balance -= amount;
        }else{
            System.out.println("Ei riittävästi rahaa.");
        }
    }
}

class CreditAccount extends Account{
    
    private int creditLimit;
    
    public CreditAccount(){
        super();
        creditLimit = 0;
    }
    
    public CreditAccount(String n, int b, int cL){
        super(n, b);
        creditLimit = cL;
        System.out.println("Luottorajaksi asetettu " + creditLimit);
    }
    
    public void withdrawMoney(int amount){
        if(balance + creditLimit - amount >= 0){
            balance -= amount;
        }else{
            System.out.println("Ei riittävästi luottoa.");
        }
    }
    
    public int getCreditLimit(){
        return creditLimit;
    }
}
