package mainclass;

//Toivo Mattila (0452499)
//2016-09-17
import java.util.Scanner;

public class Dog {
    private String name, says;
    
    public Dog(String n){
        n.trim();
        if(n.isEmpty()){
            name = "Doge";
        }else{
            name = n;
        }
        says = "Much wow!";
        System.out.println("Hei, nimeni on " + name);
    }
    
    public void speak(String s){
        String temp;
        Scanner scan = new Scanner(s);

        do{
            temp = scan.next();
            if(temp.equalsIgnoreCase("true") || temp.equalsIgnoreCase("false")){
                System.out.println("Such boolean: " + temp);
            }else if(temp.matches("\\d+")){
                System.out.println("Such integer: " + temp);
            }else{
                System.out.println(temp);
            }
        }while(scan.hasNext());
    }
}
