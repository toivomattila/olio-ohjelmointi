//Toivo Mattila (0452499)
//2016-10-12

package mainclass;

import java.util.ArrayList;

public class Bank {
    
    private ArrayList<Account> accounts;
    
    public Bank(){
        accounts = new ArrayList();
    }
    
    public void addCommonAccount(String accountNo, int moneyAmount){
        accounts.add(new CommonAccount(accountNo, moneyAmount));
        System.out.println("Tili luotu.");
    }
    
    public void addCreditAccount(String accountNo, int moneyAmount, int creditLimit){
        accounts.add(new CreditAccount(accountNo, moneyAmount, creditLimit));
        System.out.println("Tili luotu.");
    }
    
    public void depositMoney(String accountNo, int moneyAmount){
        for(int i = 0; i < accounts.size(); i++){
            if(accountNo.equals(accounts.get(i).getAccountNo())){
                accounts.get(i).depositMoney(moneyAmount);
                break;
            }
        }
    }
    
    public void withdrawMoney(String accountNo, int moneyAmount){
        for(int i = 0; i < accounts.size(); i++){
            if(accountNo.equals(accounts.get(i).getAccountNo())){
                accounts.get(i).withdrawMoney(moneyAmount);
                break;
            }
        }
    }
    
    public void deleteAccount(String accountNo){
        for(int i = 0; i < accounts.size(); i++){
            if(accountNo.equals(accounts.get(i).getAccountNo())){
                accounts.remove(i);
                break;
            }
        }
        System.out.println("Tili poistettu.");
    }
    
    public void printAccount(String accountNo){
        for(int i = 0; i < accounts.size(); i++){
            if(accountNo.equals(accounts.get(i).getAccountNo())){
                System.out.print("Tilinumero: " + accountNo);
                System.out.print(" Tilillä rahaa: " + accounts.get(i).getBalance());
                if(accounts.get(i) instanceof CreditAccount){
                    System.out.print(" Luottoraja: " + accounts.get(i).getCreditLimit());
                }
                System.out.println();
                return;
            }
        }
        System.out.println("Tiliä ei löytynyt.");
    }
    
    public void printAllAccounts(){
        System.out.println("Kaikki tilit:");
        for(int i = 0; i < accounts.size(); i++){
            printAccount(accounts.get(i).getAccountNo());
        }
    }
}