//Toivo Mattila (0452499)
//2016-10-05

package mainclass;

class Body{
    public Body(){
        printSelf();
    }
    
    public void printSelf(){
        System.out.println("Body");
    }
}

class Chassis{
    public Chassis(){
        printSelf();
    }
    public void printSelf(){
        System.out.println("Chassis");
    }
}

class Engine{
    public Engine(){
        printSelf();
    }
    public void printSelf(){
        System.out.println("Engine");
    }
}

class Wheel{
    public Wheel(){
        printSelf();
    }
    public void printSelf(){
        System.out.println("Wheel");
    }
}

public class Car {
    
    private Body body;
    private Chassis chassis;
    private Engine engine;
    private Wheel[] wheels;
    
    public Car(){
        System.out.print("Valmistetaan: ");
        body = new Body();
        System.out.print("Valmistetaan: ");
        chassis = new Chassis();
        System.out.print("Valmistetaan: ");
        engine = new Engine();
        wheels = new Wheel[4];
        for(int i = 0; i < 4; i++){
            System.out.print("Valmistetaan: ");
            wheels[i] = new Wheel();
        }
    }
    
    public void print(){
        System.out.println("Autoon kuuluu:");
        
        System.out.print("\t");
        body.printSelf();
        
        System.out.print("\t");
        chassis.printSelf();
        
        System.out.print("\t");
        engine.printSelf();
        
        System.out.print("\t4 ");
        wheels[0].printSelf();
    }
}
