//Toivo Mattila (0452499)
//2016-10-05

package mainclass;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;
import java.util.zip.ZipFile;
import java.util.zip.ZipInputStream;

public class ReadAndWriteIO {
    
    public ReadAndWriteIO(){
    
    }
    
    public void readAndWrite(String fileIn, String fileOut, int maxLen, String pattern) throws FileNotFoundException, IOException{
        //writeFile(fileOut, readFile(fileIn));
        BufferedReader in = new BufferedReader(new FileReader(fileIn));
        BufferedWriter out = new BufferedWriter(new FileWriter(fileOut));
        String temp = "";
        while((temp = in.readLine()) != null){
            temp = temp.trim();
            if(temp.length() < maxLen && !temp.isEmpty()){
                if(pattern == "" || temp.contains(pattern)){
                    out.write(temp);
                    out.newLine();
                }
            }
        }
        in.close();
        out.close();
    }
    
    public String readFile(String file) throws FileNotFoundException, IOException{
        BufferedReader in = new BufferedReader(new FileReader(file));
        String whole_text = "";
        String temp = "";
        while((temp = in.readLine()) != null){
            whole_text += temp;
            whole_text += "\n";
        }
        in.close();
        return whole_text;
    }
    
    public void writeFile(String file, String text) throws IOException{
        BufferedWriter out = new BufferedWriter(new FileWriter(file));
        out.write(text);
        out.close();
    }
    
    public String readTextInZip(String file) throws IOException{
        ZipFile zip = new ZipFile(file);
        Scanner scan = new Scanner(zip.getInputStream(zip.getEntry(zip.entries().nextElement().getName())));
        String whole_file = "";
        while(scan.hasNextLine()){
            whole_file += scan.nextLine();
            whole_file += "\n";
        }
        zip.close();
        return whole_file;
    }
}
