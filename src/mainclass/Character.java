//Toivo Mattila (0452499)
//2016-10-06

package mainclass;

class WeaponBehavior{
    public WeaponBehavior(){}
    public void useWeapon(){}
    public void printSelf(){
        System.out.print("Weapon");
    }
}

class SwordBehavior extends WeaponBehavior{
    public void printSelf(){
        System.out.print("Sword");
    }
}

class KnifeBehavior extends WeaponBehavior{
    public void printSelf(){
        System.out.print("Knife");
    }
}

class ClubBehavior extends WeaponBehavior{
    public void printSelf(){
        System.out.print("Club");
    }
}

class AxeBehavior extends WeaponBehavior{
    public void printSelf(){
        System.out.print("Axe");
    }
}

public class Character {
    
    public WeaponBehavior weapon;
    
    public Character(){
        selectWeapon(1);
    }
    
    public Character(int weaponChoice){
        selectWeapon(weaponChoice);
    }
    
    public void selectWeapon(int weaponChoice){
        if(weaponChoice == 1){
            weapon = new KnifeBehavior();
        }else if(weaponChoice == 2){
            weapon = new AxeBehavior();
        }else if(weaponChoice == 3){
            weapon = new SwordBehavior();
        }else if(weaponChoice == 4){
            weapon = new ClubBehavior();
        }else{
            weapon = new KnifeBehavior();
        }
    }
    
    public void printSelf(){
        System.out.print("Character");
    }
    
    public void fight(){
        printSelf();
        System.out.print(" tappelee aseella ");
        weapon.printSelf();
        System.out.println("");
    }
}

class King extends Character{
    
    public King(int weaponChoice){
        super(weaponChoice);
    }
    
    public void printSelf(){
        System.out.print("King");
    }
}

class Queen extends Character{
   
    public Queen(int weaponChoice){
        super(weaponChoice);
    }
    
    public void printSelf(){
        System.out.print("Queen");
    }
}

class Knight extends Character{
    
    public Knight(int weaponChoice){
        super(weaponChoice);
    }
    
    public void printSelf(){
        System.out.print("Knight");
    }
}

class Troll extends Character{
    
    public Troll(int weaponChoice){
        super(weaponChoice);
    }
    
    public void printSelf(){
        System.out.print("Troll");
    }
}
