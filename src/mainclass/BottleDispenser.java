//Toivo Mattila (0452499)
//2016-09-23

package mainclass;

import java.util.ArrayList;
import java.util.Scanner;

public class BottleDispenser {
    
    private int bottles;
    private float money;
    private ArrayList<Bottle> bottle_array;
    
    public BottleDispenser() {
        bottles = 6;
        money = 0;
        
        bottle_array = new ArrayList();
        
        bottle_array.add(new Bottle("Pepsi Max", "Pepsi", 0.3f, 0.5f, 1.8f));
        bottle_array.add(new Bottle("Pepsi Max", "Pepsi", 0.3f, 1.5f, 2.2f));
        bottle_array.add(new Bottle("Coca-Cola Zero", "Coca Cola", 0.3f, 0.5f, 2.0f));
        bottle_array.add(new Bottle("Coca-Cola Zero", "Coca Cola", 0.3f, 1.5f, 2.5f));
        bottle_array.add(new Bottle("Fanta Zero", "Coca Cola", 0.3f, 0.5f, 1.95f));
        bottle_array.add(new Bottle("Fanta Zero", "Coca Cola", 0.3f, 0.5f, 1.95f));
    }
    
    public void addMoney() {
        money += 1;
        System.out.println("Klink! Lisää rahaa laitteeseen!");
    }
    
    public void buyBottle() {
        
        Scanner scan = new Scanner(System.in);
        
        if(bottle_array.isEmpty()){
            //Print something
            return;
        }else if(money < bottle_array.get(0).getPrice()){
            System.out.println("Syötä rahaa ensin!");
            return;
        }else{
            printBottles();
            System.out.print("Valintasi: ");
            int choice = scan.nextInt();
            choice--;
            money -= bottle_array.get(choice).getPrice();
            System.out.println("KACHUNK! " + bottle_array.get(choice).getName() + " tipahti masiinasta!");
            bottle_array.remove(choice);
        }
    }
    
    public void returnMoney() {
        System.out.print("Klink klink. Sinne menivät rahat!");
        System.out.println(" Rahaa tuli ulos " + String.format("%.2f", money) + "€");
        money = 0;
    }
    
    public void printBottles(){
        for(int i = 0; i < bottle_array.size(); i++){
            System.out.println((i+1) + ". Nimi: " +  bottle_array.get(i).getName());
            System.out.print("  Koko: " + bottle_array.get(i).getSize());
            System.out.println("    Hinta: " + bottle_array.get(i).getPrice());
        }
    }
}