//Toivo Mattila (0452499)
//2016-10-12

package mainclass;

import java.util.Scanner;
import java.io.IOException;


public class Mainclass {
    
    public static void main(String[] args) throws IOException {
        /*Viikko 7*/
        Scanner scan = new Scanner(System.in);
        Bank bank = new Bank();
        int choice = -1;
        
        while(choice != 0){
            String accountNo = "";
            int moneyAmount = 0;
            int creditLimit = 0;            
            
            System.out.println("\n*** PANKKIJÄRJESTELMÄ ***");
            System.out.println("1) Lisää tavallinen tili");
            System.out.println("2) Lisää luotollinen tili");
            System.out.println("3) Tallenna tilille rahaa");
            System.out.println("4) Nosta tililtä");
            System.out.println("5) Poista tili");
            System.out.println("6) Tulosta tili");
            System.out.println("7) Tulosta kaikki tilit");
            System.out.println("0) Lopeta");
            System.out.print("Valintasi: ");
            choice = scan.nextInt();
            
            switch(choice){
                case 0:
                    break;
                case 1:
                    System.out.print("Syötä tilinumero: ");
                    accountNo = scan.next();
                    System.out.print("Syötä rahamäärä: ");
                    moneyAmount = scan.nextInt();
                    bank.addCommonAccount(accountNo, moneyAmount);
                    break;
                case 2:
                    System.out.print("Syötä tilinumero: ");
                    accountNo = scan.next();
                    System.out.print("Syötä rahamäärä: ");
                    moneyAmount = scan.nextInt();
                    System.out.print("Syötä luottoraja: ");
                    creditLimit = scan.nextInt();
                    bank.addCreditAccount(accountNo, moneyAmount, creditLimit);
                    break;
                case 3:
                    System.out.print("Syötä tilinumero: ");
                    accountNo = scan.next();
                    System.out.print("Syötä rahamäärä: ");
                    moneyAmount = scan.nextInt();
                    bank.depositMoney(accountNo, moneyAmount);
                    break;
                case 4:
                    System.out.print("Syötä tilinumero: ");
                    accountNo = scan.next();
                    System.out.print("Syötä rahamäärä: ");
                    moneyAmount = scan.nextInt();
                    bank.withdrawMoney(accountNo, moneyAmount);
                    break;
                case 5:
                    System.out.print("Syötä poistettava tilinumero: ");
                    accountNo = scan.next();
                    bank.deleteAccount(accountNo);
                    break;
                case 6:
                    System.out.print("Syötä tulostettava tilinumero: ");
                    accountNo = scan.next();
                    bank.printAccount(accountNo);
                    break;
                case 7:
                    bank.printAllAccounts();
                    break;
                default:
                    System.out.println("Valinta ei kelpaa.");
            }
        }
    }
}
