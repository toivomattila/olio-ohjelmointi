//Toivo Mattila (0452499)
//2016-09-23

package mainclass;

public class Bottle {
    private String name;
    private String manufacturer;
    private float total_energy;
    private float size;
    private float price;
    
    public Bottle(){
        name = "Pepsi Max";
        manufacturer = "Pepsi";
        total_energy = 0.3f;
        size = 0.5f;
        price = 1.8f;
        
    }
    Bottle(String Name, String manuf, float totE, float Size, float Price){
        name = Name;
        manufacturer = manuf;
        total_energy = totE;
        size = Size;
        price = Price;
    }
    
    public void setSize(float Size){
        size = Size;
    }
    public void setPrice(float Price){
        price = Price;
    }
    
    public String getName(){
        return name;
    }
    public String getManufacturer(){
        return manufacturer;
    }
    public float getEnergy(){
        return total_energy;
    }
    public float getSize(){
        return size;
    }
    public float getPrice(){
        return price;
    }
}
